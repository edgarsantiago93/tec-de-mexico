@include('includes.web.head')
@include('includes.web.nav')
<link rel="stylesheet" href="{{asset('/css/mainSlider.css')}}"/>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.2.5/css/swiper.min.css">









<div class="container-fluid mainCont">




<div class="col-md-9" id="quienes_somos">
<i class="fa fa-users cardIcon" aria-hidden="true"></i>
<h2 class="cardTitle">Quienes Somos?</h2>
<div class="bloque Tcent greyFont">
<p class="desc">
El Colegio Tecnológico de México se fundó a principios del año <span class="count">1997</span>, en respuesta a las necesidades de desarrollo, crecimiento y demanda de profesionales bien preparados.
</p>

<br/>




<p class="desc">Desde ese año, el Colegio Tecnológico de México brinda formación académica con apego a los planes y programas vigentes para el nivel medio superior del Instituto Politécnico Nacional </p>
<br/>
<p class="desc">
Con una oferta siempre creciente, hemos desarrollado planes de estudio desde preparatoria, revalidación de estudios,  hasta maestrias en línea (proximamente).
</p>

<button class="btn bluetecBg" onclick="document.querySelector('#que_ofrecemos').scrollIntoView({behavior: 'smooth'});"> Ver la oferta!</button>

</div>

</div>







<div class="col-md-3">
<i class="fa fa-flag cardIcon" aria-hidden="true"></i>
<h2 class="cardTitle">.</h2>
<div class="bloqueNum bluetecBg" style="color: #ffffff">



<h4 class="nums" style="margin-top: 37px;">Fundada</h4>
<span class="count">1997</span>

<h4 class="nums">Planteles</h4>
<span class="count">5</span>

<h4 class="nums">Alumnos</h4>
más de <span class="count">5000</span> desde 1997


</div>

</div>










<div class="col-xs-12">
<i class="fa fa-hourglass cardIcon" aria-hidden="true"></i>
<h2 class="cardTitle">Construcción</h2>
<div class="bloque">


<div class="owl-carousel owl-theme photoSlide">
    <div class="item">
    <img class="imgSlide" src="{{asset('img/slider/1.jpg')}}" alt=""/>
    </div>
    <div class="item ">
    <img class="imgSlide" src="{{asset('img/slider/2.jpg')}}" alt=""/>
    </div>
     <div class="item ">
    <img class="imgSlide" src="{{asset('img/slider/3.jpg')}}" alt=""/>
    </div>
     <div class="item ">
    <img class="imgSlide" src="{{asset('img/slider/4.jpg')}}" alt=""/>
    </div>
     <div class="item ">
    <img class="imgSlide" src="{{asset('img/slider/5.jpg')}}" alt=""/>
    </div>

</div>

</div>

</div>




<div class="container-fluid">
<br/><br/><br/><br/><br/>
<div class="col-md-4 col-xs-12" id="que_ofrecemos">
<i class="fa fa-sitemap cardIcon" aria-hidden="true"></i>
<h2 class="cardTitle">Qué Ofrecemos?</h2>
<div class="bloque Tcent greyFont">
<p class="desc">
El Colegio Tecnológico de México se fundó a principios del año <span class="count">1997</span>, en
 respuesta a las necesidades de desarrollo, crecimiento y demanda de profesionales bien preparados.
</p>

<br/>
<p class="desc">Desde ese año, el Colegio Tecnológico de México brinda formación académica
 con apego a los planes y programas vigentes para el nivel medio superior del Instituto Politécnico Nacional </p>
<br/>
<p class="desc">
Con una oferta siempre creciente, hemos desarrollado planes de estudio desde preparatoria,
revalidación de estudios,  hasta maestrias en línea (proximamente).
</p>
<button class="btn bluetecBg"> Ver la oferta!</button>
</div>
</div>





<div class="col-md-8 col-xs-12" id="que_ofrecemos">
<i class="fa fa-sitemap cardIcon" aria-hidden="true"></i>
<h2 class="cardTitle">Qué Ofrecemos?</h2>
<div class="bloque Tcent greyFont">

En el Colegio Tecnológico de México
Desde ese año, el Colegio Tecnológico de México brinda formación académica
con apego a los planes y programas vigentes para el nivel medio superior del
 Instituto Politécnico Nacional en los bachileratos tecnológicos en

Tras varios años de intensa labor, el Colegio Tecnológico de México alcanzó
su consolidación y amplía su oferta impartiendo las licenciaturas en:

Actualmente , nuestro abanico de posibilidades se abre para así dar paso a
nuevas Licenciaturas en línea:

Así como también, las Maestrías en línea:

<p class="desc">
</p>

<button class="btn bluetecBg"> Ver la oferta!</button>

</div>

</div>


</div>



@include('includes.web.footer')






<script>


            $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });

                function c() {
             $.ajax({url: "/eventos",
                method:"GET",
                {{--data:{matId:{{$pdf->id}}},--}}
                success: function(response){
                console.log(response)
                },
                error:function(){console.log(e)}});
                }



$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    items:3,
    autoplay:true,
    autoplayTimeout:5000,




    responsive : {
        // breakpoint from 0 up
        0 : {
            items : 1
        },
        // breakpoint from 480 up
        480 : {
            items : 1
        },
        // breakpoint from 768 up
        768 : {
            items : 1
        },
        1024:{
        items:2
        },
        1025:{
        items:3
        }


    }





});

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

</script>

<script>



</script>
