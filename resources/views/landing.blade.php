@include('includes.web.head')
@include('includes.web.nav')
<link rel="stylesheet" href="{{asset('/css/mainSlider.css')}}"/>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.2.5/css/swiper.min.css">









<div class="container-fluid">



<div class="owl-carousel principal owl-theme">

<div class="item slideImg" style="background: url('{{asset('img/slider/slide1.png')}}') 5% 50% / cover;">
<p>blablabala</p>
</div>





</div>

</div>



@foreach($avisos as $index=>$av)
@if($av->publicado == true && $av->tipo =='principal')
<div class="container">
<div class="alert alertPrincipal alert-{{$av->tipo_notif}}">
  <strong>{{$av->titulo}}</strong> <br/>
 <p>{{$av->mensaje}}</p><br/>
 @if($av->url != 'ns')
 <a href="http://{{$av->url}}" class="linkAviso shadow">Ver Más</a>
 @endif
</div>

</div>
@endif
@endforeach

<div class="container-fluid mainCont">


<div class="col-xs-12 col-md-6 col-lg-3">
<i class="fa fa-users cardIcon" aria-hidden="true"></i>
<h2 class="cardTitle">Nosotros!</h2>
<div class="bloque">


<div id="quienes" class="Tcent"><a href="{{url('/nosotros#quienes_somos')}}"  class="butInf1 shadow">Quiénes Somos?</a></div>
<div id="que" class="Tcent"><a href="{{url('/nosotros#que_ofrecemos')}}" class="butInf2 shadow">Qué Ofrecemos?</a></div>
<div id="ventajas" class="Tcent"><a href="{{url('/nosotros#ventajas')}}"  class="butInf3 shadow">Ventajas!</a></div>

</div>
</div>





<div class="col-xs-12 col-md-6 col-lg-6">


<i class="fa fa-graduation-cap cardIcon" aria-hidden="true"></i><h2 class="cardTitle">Oferta educativa</h2>


<div class="bloque">



<div class="owl-carousel oferta owl-theme">

    <div class="item ofertaSlide" style="background: #eda43d;">
    <h2 class="imgText" id="tag1">Preparatoria Sep</h2>
    <h3 class="imgText" id="tag2">con capacitación de traductor<br/>de inglés</h3>
    <a href="{{url('/preparatoria_sep')}}" class="slideBut bluetecBg" id="tag3">Conoce más!</a>
    </div>

    <div class="item ofertaSlide" style="background: #75263c;">
    <h2 class="imgText" id="tag1">Bachillerato IPN</h2>
    <h3 class="imgText" id="tag2">con título de técnico y cédula<br/>profesional</h3>
    <a href="{{url('/preparatoria_sep')}}" class="slideBut" style="background: #0275d8;;" id="tag3">Conoce más!</a>
    </div>

    <div class="item ofertaSlide bluetecBg">
    <h2 class="imgText" id="tag1">Licenciaturas Tec</h2>
    <h3 class="imgText" id="tag2">elige de nuestras diferentes<br/>opciones</h3>
    <a href="{{url('/preparatoria_sep')}}" class="slideBut orangeTecBg" id="tag3">Conoce más!</a>
    </div>


    <div class="item ofertaSlide" style="background: #00890d;">
    <h2 class="imgText" id="tag1">Examen Ceneval</h2>
    <h3 class="imgText" id="tag2">acredita tu bachillerato con<br/>un solo examen</h3>
    <a href="{{url('/preparatoria_sep')}}" class="slideBut orangeTecBg" id="tag3">Conoce más!</a>
    </div>

    <div class="item ofertaSlide" style="background: #40a6d2;">
    <h2 class="imgText" id="tag4">Revalidación</h2>
    <h3 class="imgText" id="tag5">termina lo que iniciaste hace tiempo</h3>
    <a href="{{url('/preparatoria_sep')}}" class="slideBut orangeTecBg" id="tag6">Conoce más!</a>
    </div>

    <div class="item ofertaSlide" style="background: #8a37de;">
    <h2 class="imgText" id="tag7">En línea!</h2>
    <h3 class="imgText" id="tag8">proximamente ofreceremos opciones<br/>a distancia</h3>
    <a href="{{url('/preparatoria_sep')}}" class="slideBut orangeTecBg" id="tag6">Conoce más!</a>
    </div>


     {{--<div class="item ofertaSlide" style="background: url('{{asset('img/slider/slide1.png')}}');">--}}

    {{--<p>blablabala</p>--}}
    {{--</div>--}}
</div>



</div>

</div>





<div class="col-xs-12 col-md-6 col-lg-3">
<i class="fa fa-newspaper-o cardIcon" aria-hidden="true"></i><h2 class="cardTitle">Avisos | Noticias</h2>

<div class="bloque">
<div class="avisosList">

<table style="width: 100%;">
@foreach($avisos as $index=>$av)
@if($av->publicado == true && $av->tipo =='evento')
<tr>

    <td><a href="http://www.google.com"> <p class="avisoInstance">{{$av->titulo}}</p></a></td>
    <td style="width: 25%;text-align: center"><span>{{$av->fechaP}}</span></td>
</tr>
@endif
@endforeach
</table>

</div>

<button class="btn" style="background: #224a90;">ver todos</button>
</div>
</div>

<div class="col-xs-12 col-md-6">
<i class="fa fa-language cardIcon" aria-hidden="true"></i><h2 class="cardTitle">Difusión Cultural</h2>

<div class="bloque">
<img src="{{asset('/media/landing.jpg')}}" alt="Ofrenda de dia de muertos" style="width: 100%"/>
</div>
</div>


<div class="col-xs-12 col-md-6">
<i class="fa fa-language cardIcon" aria-hidden="true"></i><h2 class="cardTitle">Eventos Académicos</h2>

<div class="bloque">

<video src="{{asset('/media/landing.mp4')}}" autoplay="true" muted="true" width="100%" loop="true"></video>
</div>
</div>





{{--<div class="col-xs-12 col-md-4">--}}
{{--<i class="fa fa-calendar cardIcon" aria-hidden="true"></i><h2 class="cardTitle">Calendario</h2>--}}

{{--<div class="bloque">--}}

{{--</div>--}}
{{--</div>--}}






</div>




@include('includes.web.footer')






<script>


            $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });

                function c() {
             $.ajax({url: "/eventos",
                method:"GET",
                {{--data:{matId:{{$pdf->id}}},--}}
                success: function(response){
                console.log(response)
                },
                error:function(){console.log(e)}});
                }



$('.principal').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    items:1,
    autoplay:true,
    autoplayTimeout:10000
});
$('.oferta').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    items:1,
      autoplay:true,
        autoplayTimeout:5000
});


</script>

<script>


var shown = Cookies.get('showNotif');



@foreach($avisos as $index=>$av)

@if($av->publicado == true && $av->tipo =='notif')

if(shown != 1){
setTimeout(function(){
$.notify({
	// options
	icon: 'glyphicon glyphicon-plus',
	title: '{{$av->titulo}}',
	message: '{{$av->mensaje}}',
	@if($av->url != 'ns')
	url: '{{$av->url}}',
	@endif
	target: '_blank'
},{
	// settings
	element: 'body',
	position: null,
	type: "{{$av->tipo_notif}}",
	allow_dismiss: true,
	newest_on_top: false,
	showProgressbar: true,
	placement: {
		from: "top",
		align: "center"
	},
	offset: 30,
	spacing: 10,
	z_index: 9999999999999,
	delay: 15000,
	timer: 1000,
	url_target: '_blank',
	mouse_over: null,
	animate: {
		enter: 'animated zoomInDown',
        exit: 'animated zoomOutUp'
	}
});
},{{$index+1 . '000'}});
}
@endif
@endforeach
Cookies.set('showNotif', 1, { expires: 1 });

function clearCookies(){
Cookies.remove('showNotif');

}

$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});

function bauns(){

$('#quienes').animateCss('bounce');

setTimeout(function(){
$('#que').animateCss('bounce');

},100);


setTimeout(function(){
$('#ventajas').animateCss('bounce');

},200);

}
bauns()
setInterval(function(){ bauns() }, 13000);

</script>
