
<div class=" mainLogo">
<a href="{{url('/')}}"><img class="logoPrincipal" src="{{asset('img/logotec.png')}}" alt="logo principal"/></a>

</div>

<nav class="navbar shadow navbar-default navbar-static-top">
  <div class="container" style=" margin-top: 40px">

    <div class="navbar-header">




      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>





   <div id="navbar" class="navbar-collapse collapse navbar-left">
      <ul class="nav navbar-nav">
      <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Oferta Educativa <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Preparatoria SEP</a></li>
                  <li><a href="#">Bachillerato IPN</a></li>
                  <li><a href="#">Licenciaturas</a></li>
                  <li role="separator" class="divider"></li>
                  <li class="dropdown-header">Otros</li>
                  <li><a href="#">Ceneval</a></li>
                  <li><a href="#">Revalidación</a></li> <li role="separator" class="divider"></li>
                  <li class="dropdown-header">En Línea</li>
                  <li><a href="#">Licenciaturas</a></li>
                  <li><a href="#">Maestrias</a></li>
                </ul>
              </li>
          <li class="hidden-sm hidden-md hidden-lg hidden-xl"><a href="#">Oferta Educativa</a></li>

        <li style="background: #ce6330;"><a href="#contact" style="color:white">Inscripciones</a></li>
        <li><a href="#contact">Contacto & Planteles</a></li>
        <li class="hidden-sm hidden-md hidden-lg hidden-xl"><a href="#about">Sobre Nosotros</a></li>
          <li class="hidden-sm hidden-md hidden-lg hidden-xl"><a href="#">Calendarios</a></li>
          <li class="hidden-sm hidden-md hidden-lg hidden-xl"><a href="#" class="bluetecBg" style="color: white;     background: #224a90!important;" id="padresSm">Padres & Alumnos</a></li>
          <li class="hidden-sm hidden-md hidden-lg hidden-xl"><a href="#">Oferta Educativa</a></li>

      </ul>

    </div>






    <div id="navbar" class="navbar-collapse collapse navbar-right">
      <ul class="nav navbar-nav">

          <li><a href="#">Calendarios</a></li>
        <li><a href="#about">Sobre Nosotros</a></li>


<li class="dropdown">
          <a href="#" class="dropdown-toggle padresAlumnos" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Padres & Alumnos <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li role="separator" class="divider"></li>
            <li class="dropdown-header dropHead">Alumnos</li>
            <li><a href="#">Portal para Alumnos</a></li>
            <li class="dropdown-header dropHead">Padres</li>
            <li><a href="#">Portal para padres</a></li>
          </ul>
        </li>


      </ul>

    </div>




  </div>
</nav>




