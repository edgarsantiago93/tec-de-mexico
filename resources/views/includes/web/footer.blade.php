<!--Footer-->
<footer class="page-footer  center-on-small-only" style="background: #224a90;">

    <!--Footer Links-->
    <div class="container-fluid">
        <div class="row">

            <!--First column-->
            <div class="col-md-6">
                <h5 class="title orangeTecFont">Planteles</h5>
                <p>Calzada Ignacio Zaragoza No. 1329 Col. Tepalcates, C.P. 09210 Del. Iztapalapa, México D.F.
                   Tel: 57.56.49.38 | 57.63.56.28
                   Mail: informes@tecdemexico.edu.mx</p>

                    <p>Parma No.3, Esquina Tenorios
                       Col. Ex-Hacienda de Coapa, CP.14300
                       Del. Tlalpan, México DF.
                       Tel. 56.77.99.34 | 56.77.92.09
                       Mail: informes@tecdemexico.edu.mx</p>

            </div>
            <!--/.First column-->

            <!--Second column-->
            <div class="col-md-6">
                <h5 class="title">Links de interés</h5>
                <ul>
                    <li class="footLink"><a href="http://www.seacademica.ipn.mx/RVOE/">IPN RVOE</a></li>
                    <li class="footLink"><a href="http://ipn.mx/">IPN</a></li>
                    <li class="footLink"><a href="http://sep.gob.mx/">SEP</a></li>
                    <li class="footLink"><a href="http://www.ccm.edu.mx">CCM</a></li>


                </ul>
            </div>
            <!--/.Second column-->
        </div>
    </div>
    <!--/.Footer Links-->

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">
            © 2017 Derechos Reservados Colegio Tecnológico de México A.C

        </div>
    </div>
    <!--/.Copyright-->

</footer>
<script
  src="https://code.jquery.com/jquery-3.0.0.js"
  integrity="sha256-jrPLZ+8vDxt2FnE1zvZXCkCcebI/C8Dt5xyaQBjxQIo="
  crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



  <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
  <script src="http://vjs.zencdn.net/5.19.2/video.js"></script>


<script src='{{asset('js/owl.carousel.min.js')}}'></script>
<script  src='{{asset('js/bootstrap-notify.js')}}'></script>
<script  src='{{asset('js/js.cookie.js')}}'></script>

















