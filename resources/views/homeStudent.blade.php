@include('includes.head')
@include('includes.nav')



<div class="container mainCont">




<div class="row">


<div class="col-xs-6 col-md-4">


<div class="col-xs-12 bloque width">
  <div class="col-xs-4" style="text-align: center">
  <img  class="profpic" src="{{asset('img/profilepictures/'.Auth::user()->profile_pic)}}" alt=""/>
  </div>
   <div class="col-xs-8" style="text-align: center">
  <h4 class="bloqueTitulo">{{Auth::user()->name}}</h4>
  <p class="fecha orangeTecFont"> Última actualizacion: <br/>{{$date}}</p>
  </div>
  </div>

 </div>






<div class="col-xs-6 col-md-4">

<div class="col-xs-12 bloque width">

  <div class="col-xs-4" style="text-align: center">
<i class="fa fa-graduation-cap iconblock" style="color:#66af6b;" aria-hidden="true"></i>

  </div>
   <div class="col-xs-8" style="text-align: center">
  <h4 class="bloqueTitulo">Materias cursando</h4>
  <p class="fecha orangeTecFont" style="font-size: 30px;"> 6</p>
  </div>
 </div>
  </div>


<div class="col-xs-6 col-md-4">
<div class="col-xs-12 bloque width">

  <div class="col-xs-4" style="text-align: center">
<i class="fa fa-users iconblock" style="color: #cc3e3e;" aria-hidden="true"></i>
  </div>
   <div class="col-xs-8" style="text-align: center">
  <h4 class="bloqueTitulo">Grupo actual</h4>
  <p class="fecha orangeTecFont" style="font-size: 20px;"> grupo 501</p>
  </div>
 </div>
</div>


</div>


<div class="row">

<div class="col-xs-12 col-md-5">
<div class="bloque col-xs-12">
<div class="col-xs-4">
<i class="fa fa-file-text iconblock"  style="color: #2194a9;" aria-hidden="true"></i>
</div>
<div class="col-xs-8"> <h4 style="line-height: 3;">Avisos y circulares</h4></div>


<div class="row">

<div class="col-xs-12 circularesDiv" id="avisos">



<table class="table table-striped">
  <thead>
    <tr>
      <th>Título</th>
      <th>Por</th>
      <th>Fecha</th>
      <th></th>
    </tr>
  </thead>
  <tbody>

    <tr>
      <th scope="row">Junta padres de familia</th>
      <td>Dirección</td>
      <td>27/06/17</td>
      <td class="moreInf"><a href="">ver más <i class="fa fa-plus-square" aria-hidden="true"></i></a></td>
    </tr>
    <tr>

@foreach($archivoEventos as $ae)
    <tr>
      <th scope="row">{{$ae->titulo}}</th>
      <td>{{$ae->user->name}}</td>
      <td>{{$ae->fechaParsed}}</td>
      <td class="moreInf"><a href="{{url('/circular/'.$ae->id)}}">ver más <i class="fa fa-plus-square" aria-hidden="true"></i></a></td>
    </tr>
@endforeach



  </tbody>

</table>
<h4 class="Tcent">Los avisos continuan arriba, solo se muestran los más nuevos.
    Para ver avisos  antiguos, puedes ir al inicio o da click en la flecha <br/> <i id="up" onclick="$('#avisos').scrollTop(0)" class="fa fa-arrow-circle-up orangeTecFont" aria-hidden="true"></i>
</h4>


</div>

</div>


</div>

</div>




  <div class="col-xs-12 col-md-7">

  <div class="col-xs-12 bloque">

  <div class="col-xs-12 "><div class="col-xs-4"><h4>Calendario de eventos</h4></div>
   <div class="col-xs-12 col-md-8">

   <div class="col-xs-4 eventosLabelsdiv "><h4 class="aca eventosLabels shadow">Académicos</h4></div>
   <div class="col-xs-4 eventosLabelsdiv "><h4 class="ext eventosLabels shadow">Extraordinario</h4></div>
   <div class="col-xs-4 eventosLabelsdiv"><h4 class="soc eventosLabels shadow">Social</h4></div>



   </div></div>

         <div id='calendar'></div>

</div>
  </div>
</div>



</div>


</div>
@include('includes.footer')





<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/lib/moment.min.js'></script>
<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/lib/jquery.min.js'></script>
<script src="http://fullcalendar.io/js/fullcalendar-2.1.1/lib/jquery-ui.custom.min.js"></script>
<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/fullcalendar.min.js'></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://momentjs.com/downloads/moment.min.js"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.js'></script>
        <link rel='stylesheet' href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css" />
<script src='{{asset('js/locale-all.js')}}'></script>

        <script>
            $(document).ready(function() {

            // page is now ready, initialize the calendar...

            $('#calendar').fullCalendar({
                // put your options and callbacks here
                left:   'Calendar',
                center: '',
                right:  'today prev,next',
                locale: 'es',
                events: [
                @foreach($eventos as $e)
                    {
                        title  : '{{$e->nombre}}' ,
                        start  : '{{$e->start}}',
                        @if(is_null($e->end))
                        allDay : true,
                        @else
                        end  : '{{$e->end}}',
                        @endif
                        id     : '{{$e->id}}',
                        @if($e->tipo == 'academico')
                        className: 'aca'
                        @elseif($e->tipo == 'extra')
                        className: 'ext'
                        @elseif($e->tipo == 'social')
                        className: 'soc'
                        @endif
                    },
                @endforeach
                    ],

    eventClick: function(calEvent, event) {

        alert('Event: ' + calEvent.title);
        alert(calEvent.id);
        // change the border color just for fun
        $(this).css('border-color', '#eb692c');

    }
                });

            });




            $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });

                function c() {
             $.ajax({url: "/eventos",
                method:"GET",
                {{--data:{matId:{{$pdf->id}}},--}}
                success: function(response){
                console.log(response)
                },
                error:function(){console.log(e)}});
                }

 @foreach($eventos as $e)
 console.log("{{$e->nombre}}")
                    console.log('{{$e->start}}');


            @if(is_null($e->end))
            console.log('no coso');
            @else
                                console.log('{{$e->end}}');

            @endif

                @endforeach


        </script>
<script>
$('#avisos').scrollTop($('#avisos')[0].scrollHeight);
</script>

