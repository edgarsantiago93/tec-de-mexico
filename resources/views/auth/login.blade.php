

@include('includes.head')
<div class="container mainCont">

<div class="row">
<div class="col-xs-12 Tcent">
                        <img  class="logoLogin" src="{{asset('img/logotec.png')}}" alt=""/>

</div>
</div>



    <div class="row">
        <div class="col-md-6 col-xs-12 ">
                    <form class="form" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group Tcent">


<button  type="submit" class="btn btn-warning waves-effect waves-light">Entrar</button>


                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                Olvidaste tu password?
                                </a>
                            </div>
                    </form>
                                            </div>


    <div class="col-xs-12 col-md-6">

    <!--Panel-->
    <div class="card">
        <h3 class="card-header bluetecBg white-text Tcent">Avisos Generales</h3>
        <div id="avisos" class="card-block avisos Tcent" >

        <div class="col-xs-12"><div class="col-xs-8"><h4 class="card-title">La plataforma esta en desarrollo</h4></div><div class="col-xs-4" style="margin-top: 9px;"><span class="fechaAviso"> 25/05/17</span></div></div>
        <div class="col-xs-12"><div class="col-xs-8"><h4 class="card-title">La plataforma esta en desarrollo</h4></div><div class="col-xs-4" style="margin-top: 9px;"><span class="fechaAviso"> 25/05/17</span></div></div>
        <div class="col-xs-12"><div class="col-xs-8"><h4 class="card-title">La plataforma esta en desarrollo</h4></div><div class="col-xs-4" style="margin-top: 9px;"><span class="fechaAviso"> 25/05/17</span></div></div>
        <div class="col-xs-12"><div class="col-xs-8"><h4 class="card-title">La plataforma esta en desarrollo</h4></div><div class="col-xs-4" style="margin-top: 9px;"><span class="fechaAviso"> 25/05/17</span></div></div>
        <div class="col-xs-12"><div class="col-xs-8"><h4 class="card-title">La plataforma esta en desarrollo</h4></div><div class="col-xs-4" style="margin-top: 9px;"><span class="fechaAviso"> 25/05/17</span></div></div>
        <div class="col-xs-12"><div class="col-xs-8"><h4 class="card-title">La plataforma esta en desarrollo</h4></div><div class="col-xs-4" style="margin-top: 9px;"><span class="fechaAviso"> 25/05/17</span></div></div>


        </div>

                    <a class="btn orangeTecBg">Ver más avisos</a>

    </div>
    </div>



<div>


</div>
    </div>     <!--/.Row-->





</div>

@include('includes.footer')

<script>
$('#avisos').scrollTop($('#avisos')[0].scrollHeight);
</script>














