<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'El password debe de contener al menos 6 caracteres.',
    'reset' => 'Tu password ha sido reestablecido',
    'sent' => 'Te hemos enviado el link de reset a tu correo.',
    'token' => ' El token de reset es invalido',
    'user' => "No podemos encontrar un usuario con ese correo",

];
