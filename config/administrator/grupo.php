<?php
/**
 * Created by PhpStorm.
 * User: elgal
 * Date: Mar/9/17
 * Time: 11:31
 */

return array(

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Grupos',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'grupo',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Grupo',

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'id',
        'identificador',
        'encargado_id' => array(
            'title' => "Encargado",
            'relationship' => 'encargado', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        )

    ),

    /**
     * The edit fields array
     *
     * @type array
     */
    'edit_fields' => array(
        'identificador' => array(
            'type' => 'text',
            'title' => 'Identificador de Grupo',
            'limit' => 100,
            'height' => 50,
        ),
        'encargado' => array(
            'type' => 'relationship',
            'title' => 'Encargado',
            'name_field' => 'name', //what column or accessor on the other table you want to use to represent this object
        )



    ),

    /**
     * This is where you can define the model's custom actions
     */


    'action_permissions'=> array(
//        'update' => function($model)
//        {
//            return Auth::user()->isGod();
//        }
    ),

);
