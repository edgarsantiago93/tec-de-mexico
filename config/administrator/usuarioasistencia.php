<?php
/**
 * Created by PhpStorm.
 * User: elgal
 * Date: Mar/9/17
 * Time: 11:31
 */

return array(

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Asistencias',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'asistencia',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\UsuarioAsistencia',

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(

        'id',

        'user' => array(
            'title' => "Curso al que pertenece",
            'relationship' => 'user', //this is the name of the Eloquent relationship method!
            'select' => "(:table).name",
        ),

        'materia' => array(
            'title' => "Materia",
            'relationship' => 'materia', //this is the name of the Eloquent relationship method!
            'select' => "(:table).nombre",
        ),
        'asistencia',
        'fecha',
        'created_at',
        'updated_at'


    ),

    /**
     * The edit fields array
     *
     * @type array
     */
    'edit_fields' => array(
        'user' => array(
            'type' => 'relationship',
            'title' => 'User',
            'name_field' => 'name', //what column or accessor on the other table you want to use to represent this object
        ),

        'materia' => array(
            'type' => 'relationship',
            'title' => 'Materia',
            'name_field' => 'nombre', //what column or accessor on the other table you want to use to represent this object
        ),



        'asistencia' => array(
            'type' => 'bool',
            'title' => 'Asistencia',
        ),

        'fecha' => array(
            'type' => 'datetime',
            'title' => 'Fecha',
            'date_format' => 'yy-mm-dd', //optional, will default to this value
            'time_format' => 'HH:mm:ss', 	 //optional, will default to this value
        )

    ),

    /**
     * This is where you can define the model's custom actions
     */


    'action_permissions'=> array(
//        'update' => function($model)
//        {
//            return Auth::user()->isGod();
//        }
    ),

);
