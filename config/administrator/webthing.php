<?php
/**
 * Created by PhpStorm.
 * User: elgal
 * Date: Mar/9/17
 * Time: 11:31
 */

return array(

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Web Things',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'webthing',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\WebThing',

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'id',
        'publicado',
        'titulo',
        'mensaje',
        'fecha',
        'tipo',
        'tipo_notif',
        'url'

    ),

    /**
     * The edit fields array
     *
     * @type array
     */
    'edit_fields' => array(
        'publicado' => array(
            'type' => 'bool',
            'title' => 'Publicado',
        ),
          'titulo' => array(
            'type' => 'text',
            'title' => 'Titulo',
            'limit' => 140,
            'height' => 50,
        ),
          'mensaje' => array(
            'type' => 'text',
            'title' => 'Mensaje',
            'limit' => 1000,
            'height' => 400,
        ),



        'tipo' => array(
            'type' => 'enum',
            'title' => 'Tipo',
            'options' => array(
                'notif' => 'Notificación',
                'evento' => 'Noticia/Evento',
                'principal' => 'Página Principal'
            ),
        ),


        'tipo_notif' => array(
            'type' => 'enum',
            'title' => 'Color',
            'options' => array(
                'success' => 'Verde',
                'warning' => 'Amarillo',
                'danger' => 'Rojo'
            ),
        ),


        'fecha' => array(
            'type' => 'datetime',
            'title' => 'Fecha',
            'date_format' => 'yy-mm-dd', //optional, will default to this value
            'time_format' => 'HH:mm', 	 //optional, will default to this value
        ),


        'url' => array(
        'type' => 'text',
        'title' => 'Link',
        'limit' => 200,
        'height' => 100,
        )


    ),

    /**
     * This is where you can define the model's custom actions
     */


    'action_permissions'=> array(
//        'update' => function($model)
//        {
//            return Auth::user()->isGod();
//        }
    ),

);
