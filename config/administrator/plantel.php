<?php
/**
 * Created by PhpStorm.
 * User: elgal
 * Date: Mar/9/17
 * Time: 11:31
 */

return array(

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Planteles',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'plantel',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Plantel',

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'id',
        'nombre',
        'direccion',
        'telefono',
        'encargado'

    ),

    /**
     * The edit fields array
     *
     * @type array
     */
    'edit_fields' => array(
        'nombre' => array(
            'type' => 'text',
            'title' => 'Nombre de usuario',
            'limit' => 100,
            'height' => 50,
        ),
        'direccion' => array(
            'type' => 'text',
            'title' => 'Direccion',
            'limit' => 100,
            'height' => 50,
        ),
        'telefono' => array(
            'type' => 'text',
            'title' => 'Telefono',
            'limit' => 100,
            'height' => 50,
        ),
        'encargado' => array(
            'type' => 'text',
            'title' => 'Encargado',
            'limit' => 100,
            'height' => 50,
        ),



    ),

    /**
     * This is where you can define the model's custom actions
     */


    'action_permissions'=> array(
//        'update' => function($model)
//        {
//            return Auth::user()->isGod();
//        }
    ),

);
