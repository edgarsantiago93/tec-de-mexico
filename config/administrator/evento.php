<?php
/**
 * Created by PhpStorm.
 * User: elgal
 * Date: Mar/9/17
 * Time: 11:31
 */

return array(

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Eventos',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'evento',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\Evento',

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'id',
        'nombre',
        'tipo',
        'start',
        'end'

    ),

    /**
     * The edit fields array
     *
     * @type array
     */
    'edit_fields' => array(
        'nombre' => array(
            'type' => 'text',
            'title' => 'Nombre',
            'limit' => 100,
            'height' => 50,
        ),
        'tipo' => array(
            'type' => 'enum',
            'title' => 'Tipo',
            'options' => array(
                'academico' => 'Evento Académico',
                'extra' => 'Evento Extraordinario',
                'social' => 'Evento Social'
            ),
        ),
        'start' => array(
            'type' => 'datetime',
            'title' => 'Fecha Start',
            'date_format' => 'yy-mm-dd', //optional, will default to this value
            'time_format' => 'HH:mm', 	 //optional, will default to this value
        ),
        'end' => array(
            'type' => 'datetime',
            'title' => 'Fecha End',
            'date_format' => 'yy-mm-dd', //optional, will default to this value
            'time_format' => 'HH:mm', 	 //optional, will default to this value
        )

    ),

    /**
     * This is where you can define the model's custom actions
     */


    'action_permissions'=> array(
//        'update' => function($model)
//        {
//            return Auth::user()->isGod();
//        }
    ),

);
