<?php
/**
 * Created by PhpStorm.
 * User: elgal
 * Date: Mar/9/17
 * Time: 11:31
 */

return array(

    /**
     * Model title
     *
     * @type string
     */
    'title' => 'Usuarios',

    /**
     * The singular name of your model
     *
     * @type string
     */
    'single' => 'usuario',

    /**
     * The class name of the Eloquent model that this config represents
     *
     * @type string
     */
    'model' => 'App\User',

    /**
     * The columns array
     *
     * @type array
     */
    'columns' => array(
        'id',
        'name',
        'email',
        'rol',
        'plantel_id' => array(
            'title' => "Plantel",
            'relationship' => 'plantel', //this is the name of the Eloquent relationship method!
            'select' => "(:table).nombre",
        ),
        'activo'

    ),

    /**
     * The edit fields array
     *
     * @type array
     */
    'edit_fields' => array(
        'name' => array(
            'type' => 'text',
            'title' => 'Nombre de usuario',
            'limit' => 100,
            'height' => 50,
        ),
        'email' => array(
            'type' => 'text',
            'title' => 'Email',
            'limit' => 100,
            'height' => 50,
        ),


        'rol' => array(
            'type' => 'enum',
            'title' => 'Rol',
            'options' => array(
                'maestro' => 'Maestro',
                'alumno'=>'Alumno',
                'papa' => 'Padre Familia',
                'admin'   => 'Admin'
                //GOD
            ),
        ),


        'plantel' => array(
            'type' => 'relationship',
            'title' => 'Plantel',
            'name_field' => 'nombre', //what column or accessor on the other table you want to use to represent this object
        ),



        'activo' => array(
            'type' => 'bool',
            'title' => 'Activo',
        ),



    ),

    /**
     * This is where you can define the model's custom actions
     */


    'action_permissions'=> array(
//        'update' => function($model)
//        {
//            return Auth::user()->isGod();
//        }
    ),

);
