<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['web']], function () {

    Auth::routes();


//    Route::get('/', 'WebController@home');

    Route::get('/', 'WebController@renovando');



    Route::get('/home', 'HomeController@index');

    Route::get('/logout', function(){
        Auth::logout();
        return redirect('/');
    });


Route::get('/eventos','HomeController@getEventos');
Route::get('/evento/{id}','HomeController@getEventos');





    Route::get('/date','HomeController@test');


///////////////////////PAGINA WEB


    Route::get('/', 'WebController@home');
    Route::get('/nosotros', 'WebController@nosotros');




});




