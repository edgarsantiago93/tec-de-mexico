<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioCalificacion extends Model
{
    //

    protected $table = 'usuarios_calificaciones';


    public function user(){

        return $this->belongsTo(User::class);

    }
    public function materia(){
        return $this->belongsTo(Materia::class);


    }

}
