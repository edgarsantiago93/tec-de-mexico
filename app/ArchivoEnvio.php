<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivoEnvio extends Model
{
    //
    protected $table = 'archivos_envio';

    public function user(){
        return $this->belongsTo(User::class);
    }
}
