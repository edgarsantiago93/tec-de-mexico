<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    //
    protected $table = 'grupos';

    public function encargado(){
        return $this->belongsTo(User::class);
    }


}
