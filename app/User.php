<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','rol','plantel_id','activo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];




    public function grupo(){
        return $this->hasMany(Grupo::class);
    }
    public function plantel(){
        return $this->belongsTo(Plantel::class);
    }
    public function asistencia(){
        return $this->hasMany(Asistencia::class);
    }
    public function materia(){
        return $this->hasMany(Materia::class);
    }
    public function archivos(){
        return $this->hasMany(ArchivoEnvio::class);
    }

}
