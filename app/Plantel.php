<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plantel extends Model
{
    //
    protected $table = 'planteles';
    
    public function user(){
        return $this->hasMany(User::class);
    }

}
