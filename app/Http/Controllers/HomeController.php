<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Grupo;
use App\Plantel;
use App\Materia;
use App\Evento;
use App\Asistencia;
use App\ArchivoEnvio;
use App\UsuarioAsistencia;
use App\UsuarioMateria;
use App\UsuarioCalificacion;
use App\UsuarioGrupo;
use Carbon\Carbon;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }




public function initial(){
    $eventos = $this->getEventos();
    $date = Carbon::now();
    $circulares = $this->getCirculares();
    return view('homeStudent')->with('eventos',$eventos)
                    ->with('date',$date->toDateString())
                    ->with('archivoEventos',$circulares);



}
    ///USUARIOS///

    public function getMaterias($id){
    $materias = UsuarioMateria::where('user_id',$id)->get();
        return $materias;
    }
    public function getGrupo($id){
        $grupo = UsuarioGrupo::where('user_id',$id)->first();
        return $grupo;
    }
    public function getAsistencias($id){
        $asistencias = UsuarioAsistencia::where('user_id',$id)->first();
        return $asistencias;
    }


public function getCirculares(){
    $circulares = ArchivoEnvio::all();
    foreach($circulares as $c){
        $date = Carbon::parse($c->created_at);
        $c['fechaParsed'] =  $date->toDateString();
    }

    return $circulares;
}

public function getEventos($id = null){
        $eventos  = Evento::all();

    foreach($eventos as $e){
        $e["startDate"]=$this->retDate($e->id,'start');
        $e["endDate"]=$this->retDate($e->id,'end');
        }

    if(is_null($id)){
        return $eventos;
    }

    else{
        $evento = Evento::where('id',$id)->first();
        return $evento;
    }


}



    public function retTime($id,$w){
        if($w == 'start'){
            $e = Evento::where('id',$id)->first();
            $dt = new \DateTime($e->start);
            $carbon = Carbon::instance($dt);
            return $carbon->toTimeString();
        }
        elseif($w == 'end'){
            $e = Evento::where('id',$id)->first();
            $dt = new \DateTime($e->end);
            $carbon = Carbon::instance($dt);
            return $carbon->toTimeString();
        }
    }

    public function retDate($id,$w){
        if($w == 'start'){
            $e = Evento::where('id',$id)->first();
            $dt = new \DateTime($e->start);
            $carbon = Carbon::instance($dt);
            return $carbon->toDateString();
        }
        elseif($w == 'end'){
            $e = Evento::where('id',$id)->first();
            $dt = new \DateTime($e->end);
            $carbon = Carbon::instance($dt);
            return $carbon->toDateString();
        }
    }





}
