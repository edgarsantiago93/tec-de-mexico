<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WebThing;
use Carbon\Carbon;


class WebController extends Controller
{
    //


    public function home(){
        Carbon::setToStringFormat('d-m-y');

        $avisos = WebThing::get();

        foreach($avisos as $av){
            if($av->tipo == 'evento'){
                $dt = Carbon::parse($av->fecha);
                $av['fechaP'] = $dt;

            }

        }

        return view('landing')->with('avisos',$avisos);
    }


    public function renovando(){
        return view('welcome');
    }

    public function nosotros(){
        return view('nosotros');
    }

}



