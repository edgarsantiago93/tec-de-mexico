<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioAsistencia extends Model
{
    //

    protected $table = 'usuarios_asistencias';

    public function user(){

        return $this->belongsTo(User::class);

    }
    public function materia(){
        return $this->belongsTo(Materia::class);


    }


}
