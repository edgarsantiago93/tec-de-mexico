<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioMateria extends Model
{
    //

    protected $table = 'usuarios_materias';



    public function user(){

        return $this->belongsTo(User::class);

    }
    public function materia(){
        return $this->belongsTo(Materia::class);

    }



}
