<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioGrupo extends Model
{
    //
    protected $table = 'usuarios_grupos';




    public function user(){

        return $this->belongsTo(User::class);

    }
    public function grupo(){
        return $this->belongsTo(Grupo::class);

    }


}

