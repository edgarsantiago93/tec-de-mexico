<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioMaterial extends Model
{
    //

    protected $table = 'usuarios_materiales';



    public function user(){

        return $this->belongsTo(User::class);

    }
    public function material(){
        return $this->belongsTo(ArchivoEnvio::class);

    }



}
